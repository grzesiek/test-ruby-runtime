# Ruby Runtime

This runtime is used by the [`gitlabktl`](http://gitlab.com/gitlab-org/gitlabktl/) tool to build Ruby functions.

The runtime exposes a `Dockerfile.template` file for [`gitlabktl`](http://gitlab.com/gitlab-org/gitlabktl/) to parse.

The base image of this `Dockerfile.template` file is a simple Rack server exposed by our [ruby-function-invoker](https://gitlab.com/Alexand/ruby-function-invoker#examples-of-functions).

## Requirements

[`gitlabktl`](http://gitlab.com/gitlab-org/gitlabktl/) isn't usually used directly to deploy functions. Instead, `serverless.yml` and `.gitlab-ci.yml` template files are used as explained in [GitLab Serverless documentation](https://docs.gitlab.com/ee/user/project/clusters/serverless/#deploying-functions).

For functions to be properly built, the `Dockerfile.template` requires that:

- [`gitlabktl`](http://gitlab.com/gitlab-org/gitlabktl/) sets the build context as the directory where the functions are stored. If using [GitLab Serverless](https://docs.gitlab.com/ee/user/project/clusters/serverless/#deploying-functions), use the `source:` key to define the build context.

- A Gemfile exists in this context folder. It may be empty, but must exist.

- [`gitlabktl`](http://gitlab.com/gitlab-org/gitlabktl/) sets the `FUNCTION_HANDLER` environment variable, which represents how the function will be invoked. If using [GitLab Serverless](https://docs.gitlab.com/ee/user/project/clusters/serverless/#deploying-functions), use the `handler:` key to define this variable.

## Defining functions

Currently, two types of functions are supported:

- Class-scoped
- `Object` global-scoped functions.

Instance methods are not allowed as an entrypoint for calling a function, but you can instantiate an object in a static function itself.

The following are examples of each type.

### Example class-scoped function

```ruby
class MyClass
  def my_function(event:, context:)
    { request_params: event }
  end
end
```

To use the function, set
`FUNCTION_HANDLER="MyClass.my_function"`, or with [GitLab Serverless](https://docs.gitlab.com/ee/user/project/clusters/serverless/#deploying-functions) `hander: Myclass.my_function`

### Example object global-scoped function

```ruby
def rack_request_env(event:, context:)
  context
end
```

To use the function, set
`FUNCTION_HANDLER="rack_request_env"`, or with [GitLab Serverless](https://docs.gitlab.com/ee/user/project/clusters/serverless/#deploying-functions) `hander: rack_request_env`

## Calling functions

Functions can be called either via GET or POST HTTP methods. Query string and body payload will be converted to a hash of parameters called `event`.

For example, imagine we deployed `MyClass.my_function` to "http://my_function.localhost". We could call it both ways:

```bash
curl "http://my_function.localhost/?foo=bar"
==> "{\"request_params\":{\"foo\":\"bar\"}}"
```

```bash
curl -XPOST -d "baz=qux" "http://my_function.localhost/?foo=bar"
==> "{\"request_params\":{\"foo\":\"bar\",\"baz\":\"qux\"}}"
```

Furthermore, `context` is another variable exposed to every function. It contains the Rack request environment.

## Function response

The return of every function is passed to `JSON.dump()`.
