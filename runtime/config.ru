# frozen_string_literal: true

require 'rack'
require_relative 'lib/function/invoker'
require_relative 'lib/function/loader'
require_relative 'lib/function/handler'
require_relative 'lib/function/request'

Function::Loader.load! do
  run Function::Invoker.new
end
